import 'module-alias/register';
import { server } from './server';

let runingServer = new server();

async function start() {
    await runingServer.startServer();
}

start();
