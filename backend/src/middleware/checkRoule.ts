import { IUser } from "@models/user.model";
import { Request, Response, NextFunction } from "express";

export const checkRouls = (roles: Array<string>) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    let userData: IUser = req.user as IUser;

    if (userData && userData.roles) {
      for (let i = 0; i < roles.length; i++) {
        if (userData.roles.indexOf(roles[i]) === -1) {
          console.log(`not founf roule: ${userData._id} / ${userData.email} and roule: ${roles[i]}`)
          res.status(401).send();
          return;
        }
      }
      next();
    } else {
      res.status(401).send();
    }
  };
};
