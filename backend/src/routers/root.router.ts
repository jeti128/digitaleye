import express, { Router, Request, Response } from "express";
import { userRouter } from "./user.router";
//import { userAuthedRouter } from "./user/user.authed.router";
//import { adminRouter } from "./admin/admin.router";

import path from "path";

export const rootRouter = Router();

//rootRouter.use(`/public`, express.static(path.join(__dirname, '../../data')))

rootRouter.use(`/user`, userRouter)
rootRouter.get("/", (request: Request, response: Response) => {
  response.send("Hello DigialEye!");
});
